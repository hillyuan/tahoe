/* $Id: ios_fwd_decl.h,v 1.11 2005/04/30 21:14:46 paklein Exp $ */
/* created: paklein (08/11/1999) */
/* Include this header instead of writing forward declarations */
/* explicitly. Some compilers do not allow forward declarations */
/* of stream classes. */
#ifndef _IOSFWD_H_
#define _IOSFWD_H_

#include "Environment.h"

#include <iosfwd>
using namespace std; 

#endif // _IOSFWD_H_
