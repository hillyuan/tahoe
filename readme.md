
<B>Tahoe</B>

A research-oriented, open-source, highly flexible finite element C++ code forked from http://tahoe.sourceforge.net/ . Its original source files, which seems not under active development right now, can be downloaded from <a href="http://sourceforge.net/projects/tahoe/">here</a>. This version make it possible to work on current computer environment.

<B>How to compile</B>

・ Configuration

<code>~$: ./tahoe-manager init build</code>

Choose number of your environment. If you cannot find approximate prepared environment for you, you may need add a macro file onto folder ./macros. 

・　Create makefiles and initialize modules

<code>~$: ./tahoe-manager init</code>

・　Build modules

<code>~$: ./tahoe-manager build</code>

Further hlep maybe found at the forum <a href="http://tahoe.sourceforge.net/bb/"> here </a>.
